<?php

namespace Pallino\Webpack\ViewHelpers;

/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * ###  Asset ViewHelper
 *
 * Allows inserting a javascript or css asset based on webpack manifest
 *
 * @package    Webpack
 * @subpackage ViewHelpers\Asset
 */
class AssetViewHelper extends AbstractViewHelper {

	protected static $manifestContentArray = [];
	/**
	 * As this ViewHelper renders HTML, the output must not be escaped.
	 *
	 * @var bool
	 */
	protected $escapeOutput = false;

	protected $manifestFile;
	/**
	 * Arguments array.
	 * @var array
	 * @api
	 */
	protected $arguments = [];

	/**
	 * @return void
	 */
	public function initializeArguments() {
		$this->registerArgument(
			'source',
			'string',
			'The name of file as is saved in manifest file'
		);
		$this->registerArgument(
			'manifestPath',
			'string',
			'The manifest json file path',
			false,
			"manifest.json"
		);
		$this->registerArgument(
			'type',
			'string',
			'indicates the type of assets, possible values are: js, css',
			false,
			"js"
		);
		$this->registerArgument(
			'inheader',
			'boolean',
			'indicates if the javascript will be inserted in header block',
			false,
			"false"
		);
		$this->registerArgument(
			'infooter',
			'boolean',
			'indicates if the javascript will be inserted in footer html part',
			false,
			"false"
		);
	}

	/**
	 * This function insert the javascript or css link
	 *
	 * @return string
	 */
	public function render(): string {
		$path = $this->getAsset();
		/** @var \TYPO3\CMS\Core\Page\PageRenderer $pageRenderer */
		$pageRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class);
		switch($this->arguments['type']) {
			case 'js':
				if($this->arguments['inheader']) {
					$pageRenderer->addJsFile($path);
				} elseif($this->arguments['infooter']) {
					$pageRenderer->addJsFooterFile($path);
				} else {
					return '<script type="text/javascript" src="'.$path.'"></script>';
				}
				return '';
				break;
			case 'css':
				$pageRenderer->addCssFile($path);
				return '';
				break;
		}
		return '';
	}

	/**
	 * This function looks for the source argument in manifest file and returns the destination path
	 *
	 * @return string
	 * @throws \TYPO3\CMS\Core\Exception
	 */
	protected function getAsset(): string {
		if(isset(static::$manifestContentArray[$this->arguments['manifestPath']])) {
			if(isset(static::$manifestContentArray[$this->arguments['manifestPath']][$this->arguments['source']])) {
				return static::$manifestContentArray[$this->arguments['manifestPath']][$this->arguments['source']];
			}
			throw new Exception('script not found in manifest');
		}
		if(file_exists($this->manifestFile = PATH_site.$this->arguments['manifestPath'])) {
			$jsonData = file_get_contents($this->manifestFile);
		} elseif(file_exists($this->manifestFile = GeneralUtility::getFileAbsFileName($this->arguments['manifestPath']))) {
			$jsonData = file_get_contents($this->manifestFile);
		} else {
			throw new Exception('manifest file not found');
		}
		$assetsList = json_decode($jsonData,true);
		if(is_array($assetsList)) {
			static::$manifestContentArray[$this->arguments['manifestPath']] = $assetsList;
			if(isset(static::$manifestContentArray[$this->arguments['manifestPath']][$this->arguments['source']])) {
				return static::$manifestContentArray[$this->arguments['manifestPath']][$this->arguments['source']];
			}
			throw new Exception('script not found in manifest');
		}
		return '';
	}
}